from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class Post(models.Model):
    title = models.CharField(_('Title'), max_length=255)
    text = models.TextField(_('Text'))
    posted_at = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.title

    def get_absolute_url(self) -> str:
        return reverse('post-detail', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('-posted_at',)
