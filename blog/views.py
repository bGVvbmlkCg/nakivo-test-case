from django.views.generic import DetailView

from blog.models import Post
from common.views import AuthorityCreateView, AuthorityUpdateView, SearchListView


class PostListView(SearchListView):
    queryset = Post.objects.all()
    search_fields = ('title', 'text')


class PostDetailView(DetailView):
    queryset = Post.objects.all()


class PostCreateView(AuthorityCreateView):
    queryset = Post.objects.all()
    fields = ('title', 'text')
    template_name = 'blog/post_form.html'
    authority_field_name = 'author'


class PostUpdateView(AuthorityUpdateView):
    queryset = Post.objects.all()
    fields = ('title', 'text')
    authority_field_name = 'author'
