from django.urls import path

from blog.views import PostCreateView, PostDetailView, PostListView, PostUpdateView

urlpatterns = [
    path('', PostListView.as_view(), name='post-list'),
    path('<int:pk>', PostDetailView.as_view(), name='post-detail'),
    path('edit/<int:pk>', PostUpdateView.as_view(), name='post-update'),
    path('create/', PostCreateView.as_view(), name='post-create'),
]
