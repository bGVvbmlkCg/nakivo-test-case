from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class Product(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    description = models.TextField(_('Description'))
    price = models.PositiveIntegerField(_('Price in cents'))
    creator = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    @property
    def price_in_dollars(self) -> float:
        return self.price / 100

    def get_absolute_url(self) -> str:
        return reverse('product-detail', args=(self.pk,))

    def __str__(self) -> str:
        return self.name

    class Meta:
        ordering = ('-pk',)
