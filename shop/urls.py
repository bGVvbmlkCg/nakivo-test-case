from django.urls import path

from shop.views import ProductsCreateView, ProductsDetailView, ProductsListView, ProductsUpdateView

urlpatterns = [
    path('products', ProductsListView.as_view(), name='product-list'),
    path('products/<int:pk>', ProductsDetailView.as_view(), name='product-detail'),
    path('products/edit/<int:pk>', ProductsUpdateView.as_view(), name='product-edit'),
    path('products/create/', ProductsCreateView.as_view(), name='product-create'),
]
