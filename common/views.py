import typing

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import PermissionDenied
from django.db import models
from django.views.generic import CreateView, ListView, UpdateView

from common.services import AuthorityObjectPermission, ModelType, SearchService, SearchString


class AuthorityCreateView(LoginRequiredMixin, CreateView):
    authority_field_name: str = 'creator'

    def get_authority_field_name(self) -> str:
        return self.authority_field_name

    def set_default_instance(self, kwargs: dict) -> None:
        if not kwargs.get('instance'):
            kwargs['instance'] = self.queryset.model()

    def get_author(self) -> AbstractUser:
        return self.request.user

    def set_kwargs_instance_creator(self, kwargs: dict) -> None:
        setattr(kwargs['instance'], self.get_authority_field_name(), self.get_author())

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        self.set_default_instance(kwargs)
        self.set_kwargs_instance_creator(kwargs)

        return kwargs


class AuthorityUpdateView(LoginRequiredMixin, UpdateView):
    permission_denied_exception: Exception = PermissionDenied()
    permissions: typing.Tuple[typing.Type[AuthorityObjectPermission]] = (AuthorityObjectPermission,)
    authority_field_name: str = 'creator'

    def get_user(self) -> AbstractUser:
        return self.request.user

    def check_with_permission(self, permission_checker: AuthorityObjectPermission, _raise: bool):
        if not permission_checker.check(self.get_user()):
            if _raise:
                raise self.permission_denied_exception
            return False

    def check_permissions(self, obj: ModelType, _raise: bool = True) -> bool:
        for permission in self.permissions:
            permission_checker = permission(obj, self.authority_field_name)

            if not self.check_with_permission(permission_checker, _raise):
                return False

    def get_object(self, queryset=None):
        obj: ModelType = super().get_object(queryset)
        self.check_permissions(obj)

        return obj


class SearchListView(ListView):
    search_fields: typing.Tuple[str]
    search_query_param: str = 'search'

    def get_search_value(self) -> typing.Optional[str]:
        return self.request.GET.get(self.search_query_param)

    def get_search_string(self) -> SearchString:
        return SearchString(self.get_search_value())

    def get_search_service(self) -> SearchService:
        return SearchService(
            self.queryset,
            self.get_search_string(),
            self.search_fields
        )

    def get_queryset(self) -> models.QuerySet:
        if not self.get_search_value():
            return self.queryset

        return self.get_search_service().get_search_queryset()

    def get_context_data(self, *, object_list=None, **kwargs) -> dict:
        context: dict = super().get_context_data(object_list=object_list, **kwargs)

        context.setdefault('is_searchable', True)

        return context
