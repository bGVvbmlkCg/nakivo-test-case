Application uses SQLite database by default. You can set DSN of your database by DATABASE_URL environment variable.

By default, created super user with next credentials:
* username: superuser
* password: Test1234$

Be careful with credentials if you'll use Docker compose, check the actual credentials in docker-compose.develop.yml.